# frozen_string_literal: true

class User < ActiveRecord::Base
  rolify
  extend Devise::Models
  # Include default devise modules. Others available are:
 
  devise :database_authenticatable, 
          :registerable,
          :recoverable, 
          :rememberable, 
          :validatable, 
          :trackable,
          :confirmable,
          :lockable, 
          :timeoutable, 
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  attribute :rules
  def rules
    Ability.new(self).to_list
  end

  after_find do |user|
    user.rules = rules
  end
end
